package fp_in_scala.chapter2

import org.scalatest.{FunSuite, Matchers}

/**
  * Created by karellen on 2016. 12. 11..
  */
class Exercise_2_5 extends FunSuite with Matchers {

  def compose[A, B, C](f : B => C, g : A => B) : A => C = (a : A) => f(g(a))

  test("compose") {
    case class A(s : String)

    val func: (Int) => A =
      compose[Int, String, A]((b : String) => A(b), (a : Int) => a.toString)
  }
}
