package fp_in_scala.chapter2

import org.scalatest.{FunSuite, Matchers}

/**
  * Created by karellen on 2016. 12. 11..
  */
class Exercise_2_3 extends FunSuite with Matchers {

  def curry[A, B, C](f : (A, B) => C): A => (B => C) =
    (a : A) => (b : B) => f(a, b)

  test("curry") {
    case class A(n : Int, s : String)

    val func2: (Int) => (String) => A =
      curry[Int, String, A]((a, b) => A(a, b))
  }
}
