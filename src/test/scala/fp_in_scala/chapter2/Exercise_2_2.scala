package fp_in_scala.chapter2

import org.scalatest.{FunSuite, Matchers}

import scala.collection.immutable.::

/**
  * Created by karellen on 2016. 12. 11..
  */
class Exercise_2_2 extends FunSuite with Matchers {

  def isSorted[A](as: List[A], ordered: (A, A) => Boolean): Boolean = as match {
    case head :: tail =>
      tail.foldLeft((true, head)){ case ((order, pre), cur) => (order && ordered(pre, cur), cur) }._1
    case _ => true
  }

  test("isSorted") {
    isSorted[Int](Nil, (a, b) => a < b) shouldBe true
    isSorted[Int](List(1), (a, b) => a < b) shouldBe true

    isSorted[Int](List(1,2,3,4), (a, b) => a < b) shouldBe true
    isSorted[Int](List(4,3,2,1), (a, b) => a > b) shouldBe true
    isSorted[Int](List(1,4,3,2), (a, b) => a < b) shouldBe false

    isSorted[String](List("가","나","다","라"), (a, b) => a < b) shouldBe true
    isSorted[String](List("다","나","가","라"), (a, b) => a < b) shouldBe false
  }
}
