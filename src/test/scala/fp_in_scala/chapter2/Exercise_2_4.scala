package fp_in_scala.chapter2

import org.scalatest.{FunSuite, Matchers}

/**
  * Created by karellen on 2016. 12. 11..
  */
class Exercise_2_4 extends FunSuite with Matchers {

  def uncurry[A, B, C](f: A => B => C): (A, B) => C =
    (a : A, b : B) => f(a)(b)

  test("uncurry") {
    case class A(n : Int, s : String)

    val func: (Int, String) => A =
      uncurry[Int, String, A]((a : Int) => (b : String) => A(a, b))
  }
}
