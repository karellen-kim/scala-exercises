package fp_in_scala.chapter3

import org.scalatest.{FunSuite, Matchers}

import scala.annotation.tailrec

/**
  * Created by karellen on 2016. 12. 18..
  */
class Exercise_3_5 extends FunSuite with Matchers {

  def dropWhile[A](list : List[A], f : A => Boolean) : List[A] = {

    @tailrec
    def _dropWhile(l: List[A], pos : Int) : List[A] = pos < list.length && f(list(pos)) match {
      case true => _dropWhile(l.tail, pos + 1)
      case false => l
    }
    _dropWhile(list, 0)
  }

  test("dropWhile") {
    //dropWhile(List(1,2,3,4,5), (n : Int) => n < 0) shouldBe List(1,2,3,4,5)
    dropWhile(List(1,2,3,4,5), (n : Int) => n < 3) shouldBe List(3,4,5)
    dropWhile(List(1,2,3,4,5), (n : Int) => n < 10) shouldBe Nil
  }
}
