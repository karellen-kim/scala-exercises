package fp_in_scala.chapter3

import org.scalatest.{FunSuite, Matchers}

import scala.annotation.tailrec

/**
  * Created by karellen on 2016. 12. 18..
  */
class Exercise_3_2 extends FunSuite with Matchers {

  def tail[A](list: List[A]): List[A] = {
    @tailrec
    def _tail(tail: List[A], pos : Int) : List[A] = pos match {
      case 0 => tail
      case 1 => tail
      case _ => _tail(list(pos - 1) :: tail, pos - 1)
    }

    _tail(Nil, list.length)
  }

  test("tail") {
    tail(List(1,2,3,4,5)) shouldBe List(2,3,4,5)
  }
}
