package fp_in_scala.chapter3

import org.scalatest.{FunSuite, Matchers}

import scala.annotation.tailrec

/**
  * Created by karellen on 2016. 12. 18..
  */
class Exercise_3_4 extends FunSuite with Matchers {

  def drop[A](list: List[A], n: Int): List[A] = {
    @tailrec
    def _drop(l: List[A], pos: Int): List[A] = (n - pos) match {
      case m if m <= 0 => l
      case _ => _drop(l.tail, pos + 1)
    }
    _drop(list, 0)
  }

  test("drop") {
    drop(List(1,2,3,4,5),0) shouldBe List(1,2,3,4,5)
    drop(List(1,2,3,4,5),2) shouldBe List(3,4,5)
    drop(List(1,2,3,4,5),5) shouldBe Nil
  }
}
