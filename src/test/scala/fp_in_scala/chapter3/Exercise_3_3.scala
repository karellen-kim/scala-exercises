package fp_in_scala.chapter3

import org.scalatest.{FunSuite, Matchers}

import scala.annotation.tailrec

/**
  * Created by karellen on 2016. 12. 18..
  */
class Exercise_3_3 extends FunSuite with Matchers {

  def setHead[A](list: List[A], item: A): List[A] = {
    @tailrec
    def _setHead(tail: List[A], pos: Int): List[A] = pos match {
      case 0 => tail
      case 1 => item :: tail
      case _ => _setHead(list(pos - 1) :: tail, pos - 1)
    }
    _setHead(Nil, list.length)
  }

  test("tail") {
    setHead(List(1,2,3,4,5), 10) shouldBe List(10,2,3,4,5)
  }
}
